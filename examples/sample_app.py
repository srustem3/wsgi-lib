import json

from wsgi_lib.response import RedirectResponse, UnicodeResponse, DefaultResponse
from wsgi_lib.routing import Router


def admin_view(request):
    return DefaultResponse(body=b'Admin content')


def users_view(request, user_id):
    return UnicodeResponse(body=f'User content for user_id={user_id}')


def hello_redirect_view(request, nested_id, hello_id):
    return RedirectResponse(redirect_location=f'/nested/{nested_id}/hello/')


def hello_view(request, nested_id):
    return UnicodeResponse(body=f'Nested hello URL content with nested_id={nested_id}')


def default_nested_redirect_view(request, nested_id):
    return RedirectResponse(redirect_location='/')


def main_page(request):
    value_1 = request.args.get('field_1')
    return UnicodeResponse(body=json.dumps({'json_key': 'json_value', 'field_1': value_1}), headers={
        'Accept': 'application/json'
    })


app = Router([
    (r'admin/', admin_view),
    (r'users/(?P<user_id>.+)/', users_view),
    (r'nested/(?P<nested_id>.+)/', Router([
        (r'hello/(?P<hello_id>.+)/', hello_redirect_view),
        (r'hello/', hello_view),
        (r'', default_nested_redirect_view)
    ])),
    (r'', main_page)
])
