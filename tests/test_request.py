import io

import pytest

from wsgi_lib.request import Request


@pytest.fixture()
def request_obj():
    environ = {
        'PATH_INFO': '/',
        'REQUEST_METHOD': 'GET',
        'QUERY_STRING': 'field1=value1&field2=value2&field3=value3',
        'REMOTE_ADDR': '8.8.8.8',
        'wsgi.input': io.BytesIO(b'Request Body'),
        'HTTP_HOST': '127.0.0.1:8000',
        'HTTP_USER_AGENT': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:91.0) Gecko/20100101 Firefox/91.0',
        'HTTP_ACCEPT': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
    }
    return Request(environ)


def test_request_start_line(request_obj):
    assert request_obj.path == '/'
    assert request_obj.request_method == 'GET'
    assert request_obj.query_string == 'field1=value1&field2=value2&field3=value3'


def test_request_query_string_args(request_obj):
    assert request_obj.args['field1'] == 'value1'
    assert request_obj.args['field2'] == 'value2'
    assert request_obj.args['field3'] == 'value3'


def test_request_headers(request_obj):
    assert request_obj.headers['HOST'] == '127.0.0.1:8000'
    assert request_obj.headers['USER_AGENT'] == 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:91.0)' \
                                                ' Gecko/20100101 Firefox/91.0'
    assert request_obj.headers['ACCEPT'] == 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'


def test_request_body(request_obj):
    assert request_obj.body == b'Request Body'


def test_request_additional_attrs(request_obj):
    assert request_obj.remote_address == '8.8.8.8'
