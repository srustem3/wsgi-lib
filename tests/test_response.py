from unittest.mock import Mock, call

import pytest

from wsgi_lib.response import DefaultResponse, RedirectResponse, UnicodeResponse


@pytest.mark.parametrize('status_code, expected_result', [
    (200, '200 OK'),
    (301, '301 Moved Permanently'),
    (418, "418 I'm a teapot"),
    (404, '404 Not Found')
])
def test_response_status_code_text(status_code, expected_result):
    assert DefaultResponse(status_code=status_code).status_code_text == expected_result


def test_default_response_send_response_should_return_correct_body():
    response = DefaultResponse()
    result_body = response.send_response({}, lambda *args, **kwargs: None)
    assert next(iter(result_body)) == b''


@pytest.mark.parametrize('body', [
    b'a',
    b'abc'
])
def test_default_response_with_param_body_send_response_should_return_correct_body(body):
    response = DefaultResponse(body=body)
    result_body = response.send_response({}, lambda *args, **kwargs: None)
    assert next(iter(result_body)) == body


def test_default_response_send_response_should_call_start_response():
    response = DefaultResponse()
    fake_start_response = Mock()
    response.send_response({}, fake_start_response)
    assert fake_start_response.call_args == call('200 OK', {}.items())


@pytest.mark.parametrize('headers, status_code, expected_status_code_text', [
    ({'Content-Type': 'text/html; charset=utf-8'}, 200, '200 OK'),
    ({'Date': 'Mon, 18 Jul 2016 16:06:00 GMT'}, 404, '404 Not Found')
])
def test_default_response_send_response_should_call_start_response(headers, status_code, expected_status_code_text):
    response = DefaultResponse(status_code=status_code, headers=headers)
    fake_start_response = Mock()
    response.send_response({}, fake_start_response)
    assert fake_start_response.call_args == call(expected_status_code_text, headers.items())


@pytest.mark.parametrize('headers, location', [
    ({'Content-Type': 'text/html; charset=utf-8'}, 'https://google.com'),
    ({'Date': 'Mon, 18 Jul 2016 16:06:00 GMT'}, 'https://example.com')
])
def test_redirect_response_send_response_should_call_start_response_with_location_header(headers, location):
    response = RedirectResponse(headers=headers, redirect_location=location)
    fake_start_response = Mock()
    response.send_response({}, fake_start_response)
    assert fake_start_response.call_args == call('301 Moved Permanently', {**headers, 'Location': location}.items())


@pytest.mark.parametrize('body', [
    'a',
    'abc'
])
def test_unicode_response_send_response_should_call_start_response_with_content_type_header(body):
    response = UnicodeResponse(body=body)
    result_body = response.send_response({}, lambda *args, **kwargs: None)
    assert next(iter(result_body)) == body


@pytest.mark.parametrize('headers, location', [
    ({'Example': 'header'}, 'https://google.com'),
    ({'Date': 'Mon, 18 Jul 2016 16:06:00 GMT'}, 'https://example.com')
])
def test_unicode_response_send_response_should_call_start_response_with_content_type_header(headers, location):
    response = UnicodeResponse(headers=headers)
    fake_start_response = Mock()
    response.send_response({}, fake_start_response)
    assert fake_start_response.call_args == call(
        '200 OK', {**headers, 'Content-Type': 'text/html; charset=utf-8'}.items()
    )
