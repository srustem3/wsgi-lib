import io
from unittest.mock import Mock, call

import pytest

from wsgi_lib.response import DefaultResponse
from wsgi_lib.routing import Router, NotFoundError

TEST_PARAMS_EXISTING_URLS = [
    ('/', 'view_3', {}),
    ('/admin/', 'view_1', {}),
    ('/users/123/', 'view_2', {'user_id': '123'}),
    ('/nested/5/hello/abc/', 'view_4', {'nested_id': '5', 'hello_id': 'abc'}),
    ('/nested/333/hello/', 'view_5', {'nested_id': '333'}),
    ('/nested/5/', 'view_6', {'nested_id': '5'})
]


TEST_PARAMS_NOT_EXISTING_URLS = [
    'afdsad',
    'not_found'
]


@pytest.fixture()
def router_obj():
    for i in range(6):
        globals()[f'view_{i+1}'] = Mock(return_value=DefaultResponse(body=f'view_{i+1}'.encode()))

    return Router([
        (r'admin/', globals()['view_1']),
        (r'users/(?P<user_id>.+)/', globals()['view_2']),
        (r'nested/(?P<nested_id>.+)/', Router([
            (r'hello/(?P<hello_id>.+)/', globals()['view_4']),
            (r'hello/', globals()['view_5']),
            (r'', globals()['view_6'])
        ])),
        (r'', globals()['view_3'])
    ])


@pytest.fixture()
def environ_dict():
    return {
        'PATH_INFO': '/',
        'REQUEST_METHOD': 'GET',
        'QUERY_STRING': 'field1=value1&field2=value2&field3=value3',
        'REMOTE_ADDR': '8.8.8.8',
        'wsgi.input': io.BytesIO(b'Request Body'),
        'HTTP_HOST': '127.0.0.1:8000',
        'HTTP_USER_AGENT': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:91.0) Gecko/20100101 Firefox/91.0',
        'HTTP_ACCEPT': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
    }


@pytest.mark.parametrize('path, expected_view, expected_params', TEST_PARAMS_EXISTING_URLS)
def test_getting_matching_view_func_should_return_correct_view(router_obj, path, expected_view, expected_params):
    assert router_obj._get_matching_func(path) == (globals()[expected_view], expected_params)


@pytest.mark.parametrize('path', TEST_PARAMS_NOT_EXISTING_URLS)
def test_not_existing_url_should_throw_not_found_error(router_obj, path):
    with pytest.raises(NotFoundError):
        router_obj._get_matching_func(path)


@pytest.mark.parametrize('path, expected_view_body, expected_params', TEST_PARAMS_EXISTING_URLS)
def test_calling_router_should_return_correct_body(router_obj, environ_dict, path, expected_view_body, expected_params):
    environ_dict['PATH_INFO'] = path
    assert next(iter(router_obj(environ_dict, lambda *args, **kwargs: None))) == expected_view_body


@pytest.mark.parametrize('path, expected_view_body, expected_params', TEST_PARAMS_EXISTING_URLS)
def test_calling_router_should_return_correct_body(router_obj, environ_dict, path, expected_view_body, expected_params):
    environ_dict['PATH_INFO'] = path
    assert next(iter(router_obj(environ_dict, lambda *args, **kwargs: None))) == expected_view_body.encode()


@pytest.mark.parametrize('path, expected_view_body, expected_params', TEST_PARAMS_EXISTING_URLS)
def test_calling_router_should_call_start_response(router_obj, environ_dict, path, expected_view_body, expected_params):
    environ_dict['PATH_INFO'] = path
    fake_start_response = Mock()
    next(iter(router_obj(environ_dict, fake_start_response)))
    assert fake_start_response.call_args == call('200 OK', {}.items())


@pytest.mark.parametrize('path', TEST_PARAMS_NOT_EXISTING_URLS)
def test_calling_router_should_call_start_response_for_not_existing_url(
        router_obj,
        environ_dict,
        path
):
    environ_dict['PATH_INFO'] = path
    fake_start_response = Mock()
    next(iter(router_obj(environ_dict, fake_start_response)))
    assert fake_start_response.call_args == call('404 Not Found', {}.items())
